/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInformation;

/**
 * This class represents the address.
 */
public class Address {
    private String city;
    private String state;
    private String streetAddress;
    private String zipcode;
    private String country;

    /**
     * This constructor constructs an address object.
     * @param city The city of the address.
     * @param state The state of the address.
     * @param streetAddress The street address.
     * @param zipcode The zipcode of the address.
     * @param country The country of the address.
     */
    public Address(String city, String state, String streetAddress, String zipcode, String country) {
        this.city = city;
        this.state = state;
        this.streetAddress = streetAddress;
        this.zipcode = zipcode;
        this.country = country;
    }
    
    /**
     * The second constructor constructs an empty address.
     */
    public Address() {}

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
