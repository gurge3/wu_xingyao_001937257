/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInformation;

import java.util.Date;

/**
 * This class represents a person.
 */
public class Person {
    private String picPath;
    private String dateOfBirth;
    private String name;
    private int age;
    private LicenseData licenseData;
    private CreditCard creditCard;
    private FinancialAccount financialAccount;
    private Address address;
    private Person spouse;
    
    /**
     * The default constructor which takes in personal information
     * and construct a Person object.
     * @param dateOfBirth The birthday of this person.
     * @param name The name of this person.
     * @param age The age of this person.
     * @param licenseData The license data of this person.
     * @param address The address of this person.
     * @param spouse  The spouse person of this people.
     */
    public Person(String dateOfBirth, String name, int age,
            LicenseData licenseData, Address address, Person spouse,
            FinancialAccount financialAccount, CreditCard creditCard) {
        if (age <= 0)
            throw new IllegalArgumentException("Please enter a valid age!");
        this.dateOfBirth = dateOfBirth;
        this.name = name;
        this.age = age;
        this.address = address;
        this.spouse = spouse;
        this.licenseData = licenseData;
        this.creditCard = creditCard;
        this.financialAccount = financialAccount;
    }
    
     /**
     * The second constructor which contstructs a person with empty information.
     */
    public Person() {
        licenseData = new LicenseData();
        creditCard = new CreditCard();
        financialAccount = new FinancialAccount();
        address = new Address();
    }


    public LicenseData getLicenseData() {
        return licenseData;
    }

    public void setLicenseData(LicenseData licenseData) {
        this.licenseData = licenseData;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public FinancialAccount getFinancialAccount() {
        return financialAccount;
    }

    public void setFinancialAccount(FinancialAccount financialAccount) {
        this.financialAccount = financialAccount;
    }
    
   
    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
         if (age <= 0)
            throw new IllegalArgumentException("Please enter a valid age!");
        this.age = age;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Person getSpouse() {
        return spouse;
    }

    public void setSpouse(Person spouse) {
        this.spouse = spouse;
    }
}
