/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInformation;

import java.util.Date;

/**
 * This class represents a set of credit card information.
 */
public class CreditCard {
    private String creditCardNum;
    private String cardExpDate;
    private String securityCode;
    private Address billingAddress;
    private String cardHolder;
    
    /**
     * The default constructor which constructs a credit card object.
     * @param creditCardNum The credit card number.
     * @param cardExpDate The card exp date.
     * @param securityCode The card security code.
     * @param billingAddress The billing adress of the card.
     * @param cardHolder  The card holder name.
     */
    public CreditCard(String creditCardNum, String cardExpDate, String securityCode, Address billingAddress, String cardHolder) {
        this.creditCardNum = creditCardNum;
        this.cardExpDate = cardExpDate;
        this.securityCode = securityCode;
        this.billingAddress = billingAddress;
        this.cardHolder = cardHolder;
    }
    
    /*
    The second constructor which constructs an empty credit card.
    */
    public CreditCard() {
        billingAddress = new Address();
    }

    public String getCreditCardNum() {
        return creditCardNum;
    }

    public void setCreditCardNum(String creditCardNum) {
        this.creditCardNum = creditCardNum;
    }

    public String getCardExpDate() {
        return cardExpDate;
    }

    public void setCardExpDate(String cardExpDate) {
        this.cardExpDate = cardExpDate;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public Address getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(Address billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }
}
