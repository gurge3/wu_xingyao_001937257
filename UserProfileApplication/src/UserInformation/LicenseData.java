/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInformation;

import java.util.Date;

/**
 * This class represents a license data.
 */
public class LicenseData { 
    private String licenseType;
    private String licenseNumber;
    private String licenseExpDate;
    private String licenseStartDate;
    private Address licenseAddress;
    
    /**
     * The default constructor which constructs a license.
     * @param licenseType The type of a license.
     * @param licenseNumber The number of a license.
     * @param licenseExpDate The expiration date of a license.
     * @param licenseStartDate The start date of a license.
     * @param licenseAddress  The address of a license.
     */
    public LicenseData(String licenseType, String licenseNumber, String licenseExpDate, String licenseStartDate, Address licenseAddress) {
        this.licenseType = licenseType;
        this.licenseNumber = licenseNumber;
        this.licenseExpDate = licenseExpDate;
        this.licenseStartDate = licenseStartDate;
        this.licenseAddress = licenseAddress;
    }
    
    /**
     * The second constructor which constructs an empty license.
     */
    public LicenseData(){
        licenseAddress = new Address();
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getLicenseExpDate() {
        return licenseExpDate;
    }

    public void setLicenseExpDate(String licenseExpDate) {
        this.licenseExpDate = licenseExpDate;
    }

    public String getLicenseStartDate() {
        return licenseStartDate;
    }

    public void setLicenseStartDate (String licenseStartDate) {
        this.licenseStartDate = licenseStartDate;
    }

    public Address getLicenseAddress() {
        return licenseAddress;
    }

    public void setLicenseAddress(Address licenseAddress) {
        this.licenseAddress = licenseAddress;
    }
}
