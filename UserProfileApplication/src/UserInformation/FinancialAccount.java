/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInformation;

import java.util.Date;

/**
 *
 * @author wuxingyao
 */
public class FinancialAccount {

    
    private String bankName;
    private Address bankAddress;
    private String createDate;
    private String checkingAccountNum;
    private String savingAccoundNum;
    private boolean isAccountActive;
    private CreditCard creditCardInfo;
    private String debitCardNumber;

    /**
     * This constructor constructs a banking account.
     * @param bankName
     * @param bankAddress
     * @param checkingAccountNum
     * @param isAccountActive
     * @param accountCreateDate
     * @param creditCardInfo
     * @param debitCardNumber 
     */
    public FinancialAccount(String bankName, Address bankAddress, String createDate,
            String checkingAccountNum, boolean isAccountActive, CreditCard creditCardInfo, String debitCardNumber) {
        this.bankName = bankName;
        this.bankAddress = bankAddress;
        this.checkingAccountNum = checkingAccountNum;
        this.isAccountActive = isAccountActive;
        this.creditCardInfo = creditCardInfo;
        this.debitCardNumber = debitCardNumber;
    }
    
    /**
     * This constructor constructs an empty banking account.
     */
    public FinancialAccount() {
        bankAddress = new Address();
        creditCardInfo = new CreditCard();
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Address getBankAddress() {
        return bankAddress;
    }

    public void setBankAddress(Address bankAddress) {
        this.bankAddress = bankAddress;
    }

    public String getCheckingAccountNum() {
        return checkingAccountNum;
    }

    public void setCheckingAccountNum(String checkingAccountNum) {
        this.checkingAccountNum = checkingAccountNum;
    }

    public String getSavingAccoundNum() {
        return savingAccoundNum;
    }

    public void setSavingAccoundNum(String savingAccoundNum) {
        this.savingAccoundNum = savingAccoundNum;
    }

    public boolean isIsAccountActive() {
        return isAccountActive;
    }

    public void setIsAccountActive(boolean isAccountActive) {
        this.isAccountActive = isAccountActive;
    }

    public CreditCard getCreditCardInfo() {
        return creditCardInfo;
    }

    public void setCreditCardInfo(CreditCard creditCardInfo) {
        this.creditCardInfo = creditCardInfo;
    }

    public String getDebitCardNumber() {
        return debitCardNumber;
    }

    public void setDebitCardNumber(String debitCardNumber) {
        this.debitCardNumber = debitCardNumber;
    }
    
    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
