/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Enterprise.Enterprise;
import Business.Enterprise.VaccinesSupplierDirectory;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.Role.SystemAdminRole;
import Business.UserAccount.UserAccount;
import Business.Vaccine.OrderQueue;
import Business.Vaccine.Vaccine;
import java.util.ArrayList;

/**
 *
 * @author wuxingyao
 */
public class EcoSystem extends Organization {

    private static EcoSystem business;
    private ArrayList<Network> networkList;
    private ArrayList<Vaccine> vaccineCatagory;
    private ArrayList<String> diseaseCatagory;
    private VaccinesSupplierDirectory masterVaccinesSupplierDirectory;
    private OrderQueue orderQueue;

    public static EcoSystem getInstance() {
        if (business == null) {
            business = new EcoSystem();
        }
        return business;
    }

    public Network createAndAddNetwork() {
        Network network = new Network();
        networkList.add(network);
        return network;
    }

    public ArrayList<Network> getNetworkList() {
        return networkList;
    }

    public void setNetworkList(ArrayList<Network> networkList) {
        this.networkList = networkList;
    }

    private EcoSystem() {
        super(null);
        networkList = new ArrayList<Network>();
        masterVaccinesSupplierDirectory = new VaccinesSupplierDirectory();
        orderQueue = new OrderQueue();
        vaccineCatagory = new ArrayList<Vaccine>();
        diseaseCatagory = new ArrayList<String>();
    }

    public ArrayList<Vaccine> getVaccineCatagory() {
        return vaccineCatagory;
    }

    public ArrayList<String> getDiseaseCatagory() {
        return diseaseCatagory;
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList();
        roleList.add(new SystemAdminRole());
        return roleList;
    }

    public boolean checkIfUserNameIsUnique(String userName) {
        if (!this.getUserAccountDirectory().checkIfUsernameIsUnique(userName)) {
            return false;
        }

        for (Network network : networkList) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                for (UserAccount userAccount : enterprise.getUserAccountDirectory().getUserAccountList()) {
                    if (userName.equals(userAccount.getUsername())) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

}
