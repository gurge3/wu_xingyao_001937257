/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Vaccine;

import Business.Enterprise.Enterprise;
import Business.UserAccount.UserAccount;
import java.util.ArrayList;

/**
 *
 * @author wuxingyao
 */
public class OrderQueue {
    private ArrayList<OrderRequest> list;
    
    public OrderQueue() {
        list = new ArrayList<>();
    }

    public ArrayList<OrderRequest> getList() {
        return list;
    }
    
    public OrderRequest addRequest(Vaccine vaccine, int amount, UserAccount sender, Enterprise enterprise, String status) {
        OrderRequest request = new OrderRequest();
        VaccineItem item = new VaccineItem();
        item.setAmount(amount);
        item.setVaccine(vaccine);
        request.setItem(item);
        request.setEnterpriseArea(enterprise);
        request.setSender(sender);
        request.setStatus(status);
        list.add(request);
        return request;
    }
}
