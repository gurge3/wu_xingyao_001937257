/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Vaccine;

import Business.Enterprise.Enterprise;
import Business.UserAccount.UserAccount;
import java.util.Date;

/**
 *
 * @author wuxingyao
 */
public class OrderRequest {

    private VaccineItem item;
    private UserAccount sender;
    private Enterprise enterpriseArea;
    private UserAccount receiver;
    private String status;
    private Date requestDate;
    private Date resolveDate;

    public Enterprise getEnterpriseArea() {
        return enterpriseArea;
    }

    public void setEnterpriseArea(Enterprise enterpriseArea) {
        this.enterpriseArea = enterpriseArea;
    }

    public VaccineItem getItem() {
        return item;
    }

    public void setItem(VaccineItem item) {
        this.item = item;
    }

    public UserAccount getSender() {
        return sender;
    }

    public void setSender(UserAccount sender) {
        this.sender = sender;
    }

    public UserAccount getReceiver() {
        return receiver;
    }

    public void setReceiver(UserAccount receiver) {
        this.receiver = receiver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResolveDate() {
        return resolveDate;
    }

    public void setResolveDate(Date resolveDate) {
        this.resolveDate = resolveDate;
    }

    @Override
    public String toString() {
        return getItem().getVaccine().getName();
    }

}
