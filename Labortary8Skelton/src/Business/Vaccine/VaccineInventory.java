/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Vaccine;

import java.util.ArrayList;

/**
 *
 * @author wuxingyao
 */
public class VaccineInventory {
    private ArrayList<VaccineItem> vaccineItems;

    public VaccineInventory() {
        vaccineItems = new ArrayList<VaccineItem>();
    }

    public ArrayList<VaccineItem> getVaccineItems() {
        return vaccineItems;
    }
    
    public VaccineItem addVaccineItem(Vaccine vaccine, int amount, String status) {
        VaccineItem vaccineItem = new VaccineItem();
        vaccineItem.setAmount(amount);
        vaccineItem.setVaccine(vaccine);
        vaccineItem.setStatus(status);
        vaccineItems.add(vaccineItem);
        return vaccineItem;
    }
}
