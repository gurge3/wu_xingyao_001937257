/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Vaccine;

/**
 *
 * @author wuxingyao
 */
public class Vaccine {
    private String name;
    private String disease;
    
    public Vaccine(String name) {
        this.name = name;
    }
    
     public Vaccine(String name, String disease) {
        this.name = name;
        this.disease = disease;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
