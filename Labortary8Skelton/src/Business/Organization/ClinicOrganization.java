/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.DoctorRole;
import Business.Role.LabAssistantRole;
import Business.Role.Role;
import Business.Role.VaccineInventoryManager;
import Business.Role.VaccineOrderManager;
import Business.Role.VaccineProviderManager;
import java.util.ArrayList;

/**
 *
 * @author wuxingyao
 */
public class ClinicOrganization extends Organization {

    public ClinicOrganization() {
        super(Organization.Type.Clinic.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new VaccineProviderManager());
        roles.add(new VaccineOrderManager());
        roles.add(new VaccineInventoryManager());
        roles.add(new DoctorRole());
        roles.add(new LabAssistantRole());
        return roles;
    }
    
}
