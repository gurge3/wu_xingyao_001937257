/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.DistributorInventoryRole;
import Business.Role.DoctorRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author wuxingyao
 */
public class DistributorInventoryOrganization extends Organization{
    public DistributorInventoryOrganization() {
        super(Organization.Type.DistributorInventory.getValue());
    }
    
     @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new DistributorInventoryRole());
        return roles;
    }
}
