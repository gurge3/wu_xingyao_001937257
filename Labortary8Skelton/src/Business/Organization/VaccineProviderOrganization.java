/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import Business.Role.VaccineProvider;
import java.util.ArrayList;

/**
 *
 * @author wuxingyao
 */
public class VaccineProviderOrganization extends Organization {

    public VaccineProviderOrganization() {
        super(Organization.Type.VaccineProvider.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new VaccineProvider());
        return roles;
    }

}
