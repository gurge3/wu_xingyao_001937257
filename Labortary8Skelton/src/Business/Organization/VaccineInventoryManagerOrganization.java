/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import Business.Role.VaccineInventoryManager;
import Business.Role.VaccineProviderManager;
import java.util.ArrayList;

/**
 *
 * @author wuxingyao
 */
public class VaccineInventoryManagerOrganization extends Organization {

    public VaccineInventoryManagerOrganization() {
        super(Organization.Type.VaccineInventoryManager.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new VaccineInventoryManager());
        return roles;
    }

}
