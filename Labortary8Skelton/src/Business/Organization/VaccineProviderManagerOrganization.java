/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.DoctorRole;
import Business.Role.Role;
import Business.Role.VaccineProviderManager;
import java.util.ArrayList;

/**
 *
 * @author wuxingyao
 */
public class VaccineProviderManagerOrganization extends Organization {

    public VaccineProviderManagerOrganization() {
        super(Organization.Type.VaccineProviderManager.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new VaccineProviderManager());
        return roles;
    }
    
}
