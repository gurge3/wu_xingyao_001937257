/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Employee.EmployeeDirectory;
import Business.Enterprise.VaccinesSupplierDirectory;
import Business.Role.Role;
import Business.UserAccount.UserAccountDirectory;
import Business.Vaccine.OrderQueue;
import Business.Vaccine.VaccineInventory;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public abstract class Organization {

    private String name;
    private WorkQueue workQueue;
    private OrderQueue orderQueue;
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;
    private int organizationID;
    private static int counter;
    private VaccineInventory vaccineInventory;

    public enum Type {
        Admin("Admin Organization"), Doctor("Doctor Organization"), Lab("Lab Organization"),
        Clinic("Clinic"), VaccineProviderManager("Vaccine Provider Manager"), VaccineInventoryManager("Vaccine Inventory Manager"),
        VaccineOrderManager("Vaccine Order Manager"), VaccineProvider("Vaccine Provider"), DistributorInventory("Distributor Inventory");
        private String value;

        private Type(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name;
        workQueue = new WorkQueue();
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
        orderQueue = new OrderQueue();
        vaccineInventory = new VaccineInventory();
        organizationID = counter;
        ++counter;
    }

    public VaccineInventory getVaccineInventory() {
        return vaccineInventory;
    }

    
    public abstract ArrayList<Role> getSupportedRole();

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public OrderQueue getOrderQueue() {
        return orderQueue;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    @Override
    public String toString() {
        return name;
    }
}
