/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;
import Business.Vaccine.VaccineInventory;

/**
 *
 * @author wuxingyao
 */
public abstract class Enterprise extends Organization {

    private EnterpriseType enterpriseType;
    private OrganizationDirectory organizationDirectory;
    private VaccinesSupplierDirectory localVaccinesSupplierDirectory;

    public enum EnterpriseType {
        CDC("CDC"), MAStateDept("MADept"), NYStateDept("NYDept"),
        Distributor("Distributor"), VaccinesSupplier("VaccinesSupplier");
        private String value;

        private EnterpriseType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public VaccinesSupplierDirectory getLocalVaccinesSupplierDirectory() {
        return localVaccinesSupplierDirectory;
    }

    
    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public void setEnterpriseType(EnterpriseType enterpriseType) {
        this.enterpriseType = enterpriseType;
    }

    public Enterprise(String name, EnterpriseType enterpriseType) {
        super(name);
        this.enterpriseType = enterpriseType;
        this.organizationDirectory = new OrganizationDirectory();
        this.localVaccinesSupplierDirectory = new VaccinesSupplierDirectory();
    }

    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }

    public void setOrganizationDirectory(OrganizationDirectory organizationDirectory) {
        this.organizationDirectory = organizationDirectory;
    }

    @Override
    public String toString() {
        return getName();
    }
    
    
}
