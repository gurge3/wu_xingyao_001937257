/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import java.util.ArrayList;

/**
 *
 * @author wuxingyao
 */
public class VaccinesSupplierDirectory {
    private ArrayList<Enterprise> list;
    
    public VaccinesSupplierDirectory() {
        list = new ArrayList<Enterprise>();
    }

    public ArrayList<Enterprise> getList() {
        return list;
    }
    
    public VaccinesSupplierEnterprise addSupplier(String name) {
        VaccinesSupplierEnterprise supplier = new VaccinesSupplierEnterprise(name);
        list.add(supplier);
        return supplier;
    }
}
