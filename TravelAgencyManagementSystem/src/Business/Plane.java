/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author wuxingyao
 */
public class Plane {
    private String modelNumber;
    private String serialNumber;
    private String airlinerName;

    public Plane(String modelNumber, String serialNumber, String airlinerName) {
        this.modelNumber = modelNumber;
        this.serialNumber = serialNumber;
        this.airlinerName = airlinerName;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getAirlinerName() {
        return airlinerName;
    }

    public void setAirlinerName(String airlinerName) {
        this.airlinerName = airlinerName;
    }
    
    @Override
    public String toString() {
        return serialNumber + " " + airlinerName;
    }
    
}
