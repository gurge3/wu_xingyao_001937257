/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author wuxingyao
 */
public class Seat {
    private String seatNumber;
    private Double price;
    private Customer occupant;

    public Seat(String seatNumber, Double price, Customer occupant) {
        this.seatNumber = seatNumber;
        this.price = price;
        this.occupant = occupant;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Customer getOccupant() {
        return occupant;
    }

    public void setOccupant(Customer occupant) {
        this.occupant = occupant;
    }
}
