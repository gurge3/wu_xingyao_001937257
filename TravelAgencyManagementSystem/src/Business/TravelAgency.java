/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author wuxingyao
 */
public class TravelAgency {

    private ArrayList<Airliner> airlinerList;
    private ArrayList<Order> orderList;
    private CustomerDirectory directory;
    private FlightSchedule flightSchedule;

    public TravelAgency() {
        this.airlinerList = new ArrayList<>();
        this.orderList = new ArrayList<>();
        this.directory = new CustomerDirectory();
        this.flightSchedule = new FlightSchedule();
        System.out.println("-------------------Travel Agency Management System-------------------");
        System.out.println("Loading Plane Data.....");
        importPlaneCsv();
        System.out.println("Loading Customer Data.....");
        importCustomerCsv();
        System.out.println("Loading Flight Data.....");
        importFlightCsv();
        System.out.println("Loading Order Data.....");
        importOrderCsv();
    }

    public void importPlaneCsv() {
        String csvFile = "plane.csv";
        BufferedReader br = null;
        String line = "";
        String csvSplitBy = ";";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] result = line.split(csvSplitBy);
                String modelNumber = result[0];
                String serialNumber = result[1];
                String airlinerName = result[2];
                Airliner airliner = findAirliner(airlinerName);
                if (airliner == null) {
                    airliner = createAirliner(airlinerName);
                }
                airliner.addPlane(modelNumber, serialNumber);
            }
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
    }

    private Airliner findAirliner(String name) {
        for (Airliner a : airlinerList) {
            if (a.getName().equals(name)) {
                return a;
            }
        }
        return null;
    }
    
    private Airliner createAirliner(String name) {
        Airliner newAirliner = new Airliner(name);
        airlinerList.add(newAirliner);
        return newAirliner;
    }

    private void importCustomerCsv() {
        String csvFile = "customer.csv";
        BufferedReader br = null;
        String line = "";
        String csvSplitBy = ";";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] result = line.split(csvSplitBy);
                String customerId = result[0];
                String customerName = result[1];
                directory.addCustomer(customerId, customerName);
            }
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
    }

    private void importFlightCsv() {
        String csvFile = "flight.csv";
        BufferedReader br = null;
        String line = "";
        String csvSplitBy = ";";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] result = line.split(csvSplitBy);
                String departureTime = result[0];
                String arrivalTime = result[1];
                String flightNumber = result[2];
                String serialNumber = result[3];
                String airliner = result[4];
                flightSchedule.addFlight(departureTime, arrivalTime, findAirliner(airliner).findPlaneBySerial(serialNumber),
                        flightNumber, findAirliner(airliner));
            }
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
    }

    public void importOrderCsv() {
        String csvFile = "order.csv";
        BufferedReader br = null;
        String line = "";
        String csvSplitBy = ";";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] result = line.split(csvSplitBy);
                String customerId = result[0];
                String flightNumber = result[1];
                String seatNumber = result[2];
                String seatPrice = result[3];
                Seat newSeat = new Seat(seatNumber, Double.parseDouble(seatPrice), directory.findCustomer(customerId));
                orderList.add(new Order(directory.findCustomer(customerId),
                        newSeat,   flightSchedule.findFlight(flightNumber)));
                try {
                    flightSchedule.findFlight(flightNumber).addSeatToFlight(newSeat);
                } catch (IllegalArgumentException iae) {
                    System.out.println("The seat number is invalid, please review your data file!");
                    System.exit(0);
                }
            }
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
    }

    private void calcuateRevenueByFlight() {
        System.out.println("----------------Revenue By Flight Mode----------------");
        System.out.println("Please enter the flight number: ");
        Scanner sc = new Scanner(System.in);
        String flightNumber = sc.next();
        Double revenue = 0.0;
        for (Order order : orderList) {
            if (order.getFlight().getFlightNumber().equals(flightNumber)) {
                revenue = revenue + order.getSeat().getPrice();
            }
        }
        System.out.println("Total revenue for " + flightNumber + " is: " + revenue + " Dollars.");
    }

    public void calculateRevenueByAirliner() {
        System.out.println("----------------Revenue By Airliner Mode----------------");
        System.out.println("Please enter the airliner name: ");
        Scanner sc = new Scanner(System.in);
        String airlineName = sc.next();
        Double revenue = 0.0;
        for (Order order : orderList) {
            if (order.getFlight().getAirliners().getName().equals(airlineName)) {
                revenue = revenue + order.getSeat().getPrice();
            }
        }
        System.out.println("Total revenue for " + airlineName + " is: " + revenue + " Dollars.");
    }

    public void calculateAllRevenue() {
        System.out.println("----------------Revenue By All Orders----------------");
        Double revenue = 0.0;
        for (Order order : orderList) {
            revenue = revenue + order.getSeat().getPrice();
        }
        System.out.println("Total revenue is: " + revenue + " Dollars.");
    }

    public void populateAirlinerInformation() {
        System.out.println("----------------Airline Information----------------");
        System.out.println("Please enter the name of the airliner you want to query: ");
        Scanner sc = new Scanner(System.in);
        String name = sc.next();
        for (Airliner a : airlinerList) {
            if (a.getName().equals(name)) {
                System.out.println("Plane Serial   ---   Plane Model   ---   Airliner");
                for (Plane p : a.getFleet()) {
                    System.out.println(p.getSerialNumber() + "   ---   " + p.getModelNumber() + "   ---   " + p.getAirlinerName());
                }
            }
        }
    }
        
    public void populateFlightsInformation() {
        System.out.println("----------------Flights Information----------------");
        System.out.println("Departure Time   ---   Arrival Time   ---   Carrier Plane   ---   Flight Number   ---   Airliner   ---   Seat Occupied");
        for (Flight f : flightSchedule.getList()) {
            System.out.println(f.getDepartureTime() + "   ---   " + f.getArrivalTime() + "   ---   " + f.getCarrierPlane() + "   ---   " + f.getFlightNumber() + "   ---   " + f.getAirliners().getName() + "   ---   " + f.seatOccupied());
        }
    }
    
     public void populateOrderInformation() {
        System.out.println("----------------Order Information----------------");
        System.out.println("Customer Name   ---   Flight Number   ---   Seat Number   ---   Seat Price");
        for (Order o : orderList) {
            System.out.println(o.getCustomer().getCustomerName() + "   ---   " + o.getFlight().getFlightNumber() + "   ---   " + o.getSeat().getSeatNumber() + "   ---   " + o.getSeat().getPrice());
        }
    }

    public static void main(String args[]) {
        TravelAgency ta = new TravelAgency();
        boolean exit = false;
        while (!exit) {
            System.out.println("-------------------------------");            
            System.out.println("Please select your option: ");
            System.out.println("1. Populate Airliner Information.");
            System.out.println("2. Populate Flights Information");
            System.out.println("3. Populate Orders Information");
            System.out.println("4. Calculating Revenue By Flight.");
            System.out.println("5. Calculating Revenue By Airliner.");
            System.out.println("6. Calculating All Revenue.");
            System.out.println("7. Exit System.");

            Scanner sc = new Scanner(System.in);
            String choice = sc.next();
            switch (choice) {
            case "1":
                ta.populateAirlinerInformation();
                break;
            case "2":
                ta.populateFlightsInformation();
                break;
            case "3":
                ta.populateOrderInformation();
                break;
            case "4":
                ta.calcuateRevenueByFlight();
                break;
            case "5":
                ta.calculateRevenueByAirliner();
                break;
            case "6":
                ta.calculateAllRevenue();
                break;
            case "7":
                exit = true;
                break;
            default :
                System.out.println("Please enter a valid option!");
            }
        }
    }

}
