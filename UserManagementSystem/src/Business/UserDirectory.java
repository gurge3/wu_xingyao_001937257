/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author wuxingyao
 */
public class UserDirectory {
    private ArrayList<User> listOfUsers;
    
    public UserDirectory() {
        this.listOfUsers = new ArrayList<>();
    }
    
    public User addUser() {
        User user = new User();
        listOfUsers.add(user);
        return user;
    }
    
    public User findUser(String userId) {
        for (User user : listOfUsers) {
            if (user.getUserId().equals(userId)) {
                return user;
            }
        }
        return null;
    }
    
    public User validateUser(String userId, String password) {
        User userQueried = findUser(userId);
        if (userQueried == null) {
            return null;
        } else {
            if(userQueried.validatePassword(password)) {
                return userQueried;
            } else {
                return null;
            }
        }
    }
    
    public boolean deleteUser(String userId) {
        User userToBeDeleted = findUser(userId);
        if (userToBeDeleted != null) {
            listOfUsers.remove(userToBeDeleted);
            return true;
        }
        return false;
    }
    
    public ArrayList<User> getList() {
        return listOfUsers;
    }
}
