/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author wuxingyao
 */
public class PersonDirectory {
    private ArrayList<Person> listOfPerson;
    
    public PersonDirectory() {
        listOfPerson = new ArrayList<>();
    }
    
    public Person addPerson() {
        Person person = new Person();
        listOfPerson.add(person);
        return person;
    }
    
    public Person findPerson(String lastName, String firstName) {
        for (Person person : listOfPerson) {
            if (person.getFirstName().equals(firstName) && person.getLastName().equals(lastName)) {
                return person;
            }
        }
        return null;
    }
    
    public boolean deletePerson(String lastName, String firstName) {
        Person personToBeDeleted = findPerson(lastName, firstName);
        if (personToBeDeleted != null) {
            listOfPerson.remove(personToBeDeleted);
            return true;
        }
        return false;
    }
    
    public ArrayList<Person> getList() {
        return listOfPerson;
    }
}
