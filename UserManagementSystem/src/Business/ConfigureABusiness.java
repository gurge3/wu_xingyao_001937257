/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author wuxingyao
 */
public class ConfigureABusiness {
    private PersonDirectory pd;
    private UserDirectory ud;
    
    public ConfigureABusiness() {
        pd = new PersonDirectory();
        Person p1 = new Person();
        p1.setFirstName("Lily");
        p1.setLastName("Chris");
        
        Person p2 = new Person();
        p2.setFirstName("Greg");
        p2.setLastName("Phanton");
        
        User u1 = new User();
        u1.setPerson(p1);
        u1.setUserId("lilychris");
        u1.setPassword("lilychris");
        u1.setJobRole("HR");
        u1.setIsActivate(true);
        
        User u2 = new User();
        u2.setPerson(p2);
        u2.setUserId("admin");
        u2.setPassword("admin");
        u2.setJobRole("ADMIN");
        u2.setIsActivate(true);
        
        ud = new UserDirectory();
        ud.getList().add(u1);
        ud.getList().add(u2);
        
        p1.getListOfUsers().add(u1);
        p2.getListOfUsers().add(u2);
        pd.getList().add(p1);
        pd.getList().add(p2);
        
    }

    public PersonDirectory getPd() {
        return pd;
    }

    public UserDirectory getUd() {
        return ud;
    }
}
