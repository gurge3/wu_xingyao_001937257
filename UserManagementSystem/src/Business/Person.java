/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author wuxingyao
 */
public class Person {

    private String firstName;
    private String lastName;
    private ArrayList<User> listOfUsers;

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.listOfUsers = new ArrayList<>();
    }

    public Person() {
        listOfUsers = new ArrayList<>();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void addUserToPerson(User user) {
        user.setPerson(this);
        listOfUsers.add(user);
    }

    public void deleteUserFromPerson(User user) {
        listOfUsers.remove(user);
    }

    @Override
    public String toString() {
        return lastName + " " + firstName;
    }

    public ArrayList<User> getListOfUsers() {
        return listOfUsers;
    }

}
