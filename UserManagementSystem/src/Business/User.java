/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author wuxingyao
 */
public class User {

    private String userId;
    private Person person;
    private String jobRole;
    private String password;
    private boolean isActivate;

    public User(String userId, Person person, String jobRole, String password) {
        this.userId = userId;
        this.person = person;
        this.jobRole = jobRole;
        this.password = password;
        this.isActivate = true;
    }

    /**
     * Default empty constructor.
     */
    public User() {
        this.isActivate = true;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getJobRole() {
        return jobRole;
    }

    public void setJobRole(String jobRole) {
        this.jobRole = jobRole;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = encryptPassword(password);
    }

    public boolean isActivate() {
        return isActivate;
    }

    public void setIsActivate(boolean isActivate) {
        this.isActivate = isActivate;
    }
    
    /**
     * By comparing two password encryption values we can get if the prompted
     * password is the validate password.
     *
     * @param password The password to be validated.
     * @return True if the password is correct.
     */
    public boolean validatePassword(String password) {
        return this.password.equals(encryptPassword(password));
    }

    /**
     * Encrypting the password using MD5 Algorithms.
     *
     * @param password The password to be encrypted.
     */
    private String encryptPassword(String password) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] passBytes = password.getBytes();
            md5.reset();
            byte[] digested = md5.digest(passBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < digested.length; i++) {
                sb.append(Integer.toHexString(0xff & digested[i]));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
    
    @Override
    public String toString() {
        return userId;
    }

}
