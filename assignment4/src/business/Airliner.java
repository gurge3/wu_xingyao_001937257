/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author wuxingyao
 */
public class Airliner {
    private String name;
    private ArrayList<String> fleet;
    private FlightSchedule fs;

    public Airliner(String name) {
        this.name = name;
        this.fleet = new ArrayList<>();
        this.fs = new FlightSchedule();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void addPlane(String serialNumber) {
        fleet.add(serialNumber);
    }
    
//    public Plane findPlaneBySerial(String serialNumber) {
//        for (Plane p : fleet) {
//            if (p.getSerialNumber().equals(serialNumber)) {
//                return p;
//            }
//        }
//        return null;
//    }
    
    public ArrayList<String> getFleet() {
        return fleet;
    }
    
    @Override
    public String toString() {
        return name;
    }
    
    public FlightSchedule getFs() {
        return fs;
    }

    public void setFs(FlightSchedule fs) {
        this.fs = fs;
    }

}
