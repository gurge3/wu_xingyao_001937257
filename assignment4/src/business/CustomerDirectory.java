/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author wuxingyao
 */
public class CustomerDirectory {
    private ArrayList<Customer> listOfCustomers;

    public CustomerDirectory() {
        listOfCustomers = new ArrayList<>();
    }
    
    public Customer addCustomer(String customerId, String customerName) {
        Customer customer = new Customer(customerId, customerName);
        listOfCustomers.add(customer);
        return customer;
    }
    
    public Customer findCustomer(String customerId) {
        for (Customer c : listOfCustomers) {
            if (c.getCustomerId().equals(customerId)) {
                return c;
            }
        }
        return null;
    }
}
