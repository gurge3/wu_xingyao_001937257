/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author wuxingyao
 */
public class FlightSchedule {

    private ArrayList<Flight> listOfFlights;

    public FlightSchedule() {
        listOfFlights = new ArrayList<>();
    }

    public void addFlight(String departureTime, String arrivalTime, String carrierPlane, String flightNumber, Airliner airliners, String departureCity, String arrivalCity) {
        listOfFlights.add(new Flight(departureTime, arrivalTime, carrierPlane, flightNumber, airliners, departureCity, arrivalCity));
    }

    public Flight findFlight(String flightNumber) {
        for (Flight f : listOfFlights) {
            if (f.getFlightNumber().equals(flightNumber)) {
                return f;
            }

        }
        return null;
    }
    
    public ArrayList<Flight> getList() {
        return listOfFlights;
    }
    
    public void removeFlight(String flightNumber) {
        for (Flight f : listOfFlights) {
            if (f.getFlightNumber().equals(flightNumber)) {
                listOfFlights.remove(f);
                return;
            }
        }
    }
}
