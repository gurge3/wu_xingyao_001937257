/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Random;

/**
 *
 * @author wuxingyao
 */
public class Flight {

    private String departureTime;
    private String arrivalTime;
    private String carrierPlane;
    private String flightNumber;
    private Airliner airliners;
    private Seat[][] seats;
    private String departureLocation;
    private String arrivalLocation;

    public Flight(String departureTime, String arrivalTime, String carrierPlane, String flightNumber, Airliner airliners,
            String departureLocation, String arrivalLocation) {
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.carrierPlane = carrierPlane;
        this.flightNumber = flightNumber;
        this.airliners = airliners;
        this.seats = new Seat[25][6];
        this.departureLocation = departureLocation;
        this.arrivalLocation = arrivalLocation;
        for (int i = 0; i < 25; i++) {
            for (int j = 0; j < 6; j++) {
                String seatNumber = "";
                switch (j) {
                    case 0:
                        seatNumber = "A" + i;
                        break;
                    case 1:
                        seatNumber = "B" + i;
                        break;
                    case 2:
                        seatNumber = "C" + i;
                        break;
                    case 3:
                        seatNumber = "D" + i;
                        break;
                    case 4:
                        seatNumber = "E" + i;
                        break;
                    case 5:
                        seatNumber = "F" + i;
                        break;
                }
                Random r = new Random();
                seats[i][j] = new Seat(seatNumber, 10000 * r.nextDouble(), this);
            }
        }
    }

    public Airliner getAirliners() {
        return airliners;
    }

    public void setAirliners(Airliner airliners) {
        this.airliners = airliners;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getCarrierPlane() {
        return carrierPlane;
    }

    public void setCarrierPlane(String carrierPlane) {
        this.carrierPlane = carrierPlane;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    @Override
    public String toString() {
        return flightNumber + " " + airliners.getName();
    }

    public void addSeatToFlight(Seat seat) {
        int row = 0;
        String column = "";
        if (seat.getSeatNumber().length() == 3) {
            row = Integer.parseInt(seat.getSeatNumber().substring(0, 2));
            column = seat.getSeatNumber().substring(2);
        } else {
            row = Integer.parseInt(seat.getSeatNumber().substring(0, 1));
            column = seat.getSeatNumber().substring(1);
        }
        switch (column) {
            case "A":
                seats[row][1] = seat;
                break;
            case "B":
                seats[row][2] = seat;
                break;
            case "C":
                seats[row][3] = seat;
                break;
            case "D":
                seats[row][4] = seat;
                break;
            case "E":
                seats[row][5] = seat;
                break;
            case "F":
                seats[row][6] = seat;
                break;
            default:
                System.out.println(seat.getSeatNumber());
                throw new IllegalArgumentException("Please enter a valid column number");
        }
    }

    public int seatOccupied() {
        int count = 0;
        for (int i = 0; i < 25; i++) {
            for (int j = 0; j < 6; j++) {
                if (seats[i][j] != null) {
                    count++;
                }
            }
        }
        return count;
    }

    public String getDepartureLocation() {
        return departureLocation;
    }

    public void setDepartureLocation(String departureLocation) {
        this.departureLocation = departureLocation;
    }

    public String getArrivalLocation() {
        return arrivalLocation;
    }

    public void setArrivalLocation(String arrivalLocation) {
        this.arrivalLocation = arrivalLocation;
    }

    public Seat[][] getSeats() {
        return seats;
    }
}
