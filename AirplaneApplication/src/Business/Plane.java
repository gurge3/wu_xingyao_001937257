/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * This class represents a plane.
 */
public class Plane {

    private String manufacturer;
    private String planeModel;
    private String serialNumber;
    private int seatNumber;
    private String currentLocation;
    private ArrayList<Flight> futureFlights;
    private Date maintaneseExpirationDate;
    private Date updateTime;
    private int builtYear;

    public Plane() {
        this.futureFlights = new ArrayList<>();
    }
    // This constructor is used for importing data from csv file.
     public Plane(String manufacturer, String planeModel, String serialNumber, int seatNumber, String currentLocation, Date maintaneseExpirationDate, Date updateTime, int builtYear) {
        this.manufacturer = manufacturer;
        this.planeModel = planeModel;
        this.serialNumber = serialNumber;
        this.seatNumber = seatNumber;
        this.currentLocation = currentLocation;
        this.maintaneseExpirationDate = maintaneseExpirationDate;
        this.updateTime = updateTime;
        this.builtYear = builtYear;
        futureFlights = new ArrayList<>();
    }


    public Integer getBuiltYear() {
        return builtYear;
    }

    public void setBuiltYear(Integer builtYear) {
        this.builtYear = builtYear;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getPlaneModel() {
        return planeModel;
    }

    public void setPlaneModel(String planeModel) {
        this.planeModel = planeModel;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public ArrayList<Flight> getFutureFlights() {
        return futureFlights;
    }

    public void setFutureFlights(ArrayList<Flight> futureFlights) {
        this.futureFlights = futureFlights;
    }

    public Date getMaintaneseExpirationDate() {
        return maintaneseExpirationDate;
    }

    public void setMaintaneseExpirationDate(Date maintaneseExpirationDate) {
        this.maintaneseExpirationDate = maintaneseExpirationDate;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return this.serialNumber;
    }

    public Flight createFlight() {
        Flight flight = new Flight();
        futureFlights.add(flight);
        return flight;
    }
    
    public Long getLatestArrivalTime() {
        ArrayList<Long> listOfArrivalTime = new ArrayList<>();
        for (Flight flight : futureFlights) {
            listOfArrivalTime.add(flight.getArrivalTime().getTime());
        }
        // Getting the minimum 
        Long earliestArrivalTime = Collections.max(listOfArrivalTime);
        return earliestArrivalTime;
    }
}
