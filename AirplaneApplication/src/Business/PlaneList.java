/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * This class represents the list of planes.
 */
public class PlaneList {

    private ArrayList<Plane> listOfPlanes;
    private ArrayList<String> listOfManufacturers;
    private ArrayList<Integer> listOfBuiltYear;
    private ArrayList<String> listOfModel;

    // This constructor takes the list of planes.
    public PlaneList(ArrayList<Plane> listOfPlanes) {
        this.listOfPlanes = listOfPlanes;
    }

    // Empty Constructor
    public PlaneList() {
        listOfPlanes = new ArrayList<>();
        listOfManufacturers = new ArrayList<>();
        listOfBuiltYear = new ArrayList<>();
        listOfModel = new ArrayList<>();
    }

    public void addPlane(Plane plane) {
        this.listOfPlanes.add(plane);
    }

    public void deletePlane(Plane plane) {
        if (this.listOfPlanes.contains(plane)) {
            this.listOfPlanes.remove(plane);
        } else {
            throw new IllegalArgumentException("Please verify the input!");
        }
    }

    public Plane createPlane() {
        Plane newPlane = new Plane();
        listOfPlanes.add(newPlane);
        return newPlane;
    }

    public Plane createPlaneWithParam(String manufacturer, String planeModel, String serialNumber,
            int seatNumber, String currentLocation, Date maintaneseExpirationDate, Date updateTime, int builtYear) {
        Plane newPlane = new Plane(manufacturer, planeModel, serialNumber, seatNumber, currentLocation,
                maintaneseExpirationDate, updateTime, builtYear);
        listOfPlanes.add(newPlane);
        return newPlane;
    }

    public ArrayList<Plane> getList() {
        return listOfPlanes;
    }

    public Plane retrievePlaneBySerialNum(String serialNum) {
        for (Plane plane : listOfPlanes) {
            if (plane.getSerialNumber().equals(serialNum)) {
                return plane;
            }
        }
        // Should never reaches this point.
        return null;
    }

    public void addManufacturer(String Manu) {
        if (!listOfManufacturers.contains(Manu)) {
            listOfManufacturers.add(Manu);
        }
    }

    public ArrayList<String> retriveManufacturerList() {
        return this.listOfManufacturers;
    }

    public void addBuiltYear(Integer year) {
        if (!listOfBuiltYear.contains(year)) {
            listOfBuiltYear.add(year);
        }
    }

    public ArrayList<Integer> retrieveBuiltYearList() {
        return this.listOfBuiltYear;
    }

    public void addModel(String model) {
        if (!listOfModel.contains(model)) {
            listOfModel.add(model);
        }
    }

    public ArrayList<String> retrieveModelList() {
        return this.listOfModel;
    }

    public ArrayList<Plane> findPlaneByArrivalTime(Long arrivalTime) {
        ArrayList<Plane> resultList = new ArrayList<Plane>();
        for (Plane plane : listOfPlanes) {
            if (plane.getLatestArrivalTime().equals(arrivalTime)) {
                resultList.add(plane);
            }
        }
        return resultList;
    }

    public ArrayList<Plane> findEarliestPlane() {
        ArrayList<Long> compareList = new ArrayList<>();
        for (Plane plane : listOfPlanes) {
            compareList.add(plane.getLatestArrivalTime());
        }
        Long earlistTime = Collections.min(compareList);
        return findPlaneByArrivalTime(earlistTime);
    }

}
