/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Date;

/**
 *
 * @author xingyaowu
 */
public class Flight {
    private Date arrivalTime;
    private Date departureTime;
    private String flightNumber;
    private Plane carrier;
    private String departureAirport;
    private String arrivalAirport;
    
    // Parameterized constructor
    public Flight(Date arrivalTime, Date departureTime, String flightNumber, Plane carrier, String departureAirport, String arrivalAirport) {
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
        this.flightNumber = flightNumber;
        this.carrier = carrier;
        this.departureAirport = departureAirport;
        this.arrivalAirport = arrivalAirport;
    }
    
    // Default Constructor
    public Flight() {}
    
    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Plane getCarrier() {
        return carrier;
    }

    public void setCarrier(Plane carrier) {
        this.carrier = carrier;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }
    
    @Override
    public String toString() {
        return this.flightNumber;
    }
    
    
}
